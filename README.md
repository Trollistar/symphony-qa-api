# Symphony API test automation task

---

## Introduction

These tests were created as a part of Symphony job interview.

The following requirements were presented:

* Create smoke tests to cover each of the four endpoints - **Completed**

The following nice-to-haves were presented:

* Add negative test cases - **Completed**

---

## Prerequisites

* **Java 13 JDK** - As there were no specific limitations when it comes to Java version, the second-to-latest one was used.
* **Maven** - This project relies on Maven for package management and project building.

---

## Running the tests

To execute the test suite, run the install maven goal.

```console
mvn install
```

---

## Test reports

Generated surefire html reports can be accessed under root\target\surefire-reports\index.html.

---

## Short overview of the framework structure

The key features of the framework will be listed below in no particular order.

### Api client

Api client is actually split into two layers.

**BaseApiClient** serves to abstract away the general Api functionalities. This includes forming of the request, its execution as well as deserialization of the response into proper classes.

**ReqresClient** encapsulates the specificity of Api under test. The user is then left with simple and readable methods to execute the desired calls.

### Response and request (de)serialization

Each possible response and request can be located in its respective package. They serve to utilize strong typing to make consuming the Api client even easier.

Even though it adds a bit of extra effort to the initial setup, it goes a long way to easing the long-term maintenance and making the tests enjoyable to write. 

### Test organization

Tests are organized in such a way that each relevant endpoint gets its set of tests.

This allows the user to quickly discern which of the potential endpoints, if any, is not behaving as expected.

---

## Built with

* [Okhttp](https://github.com/square/okhttp) - Decent HTTP client
* [Jackson](https://github.com/FasterXML/jackson) - Intuitive library for JSON parsing
* [TestNG](https://testng.org/doc/) - Excellent test runner for Java

---

## Author

**Slobodan Popović** - slobapop90@gmail.com

---
