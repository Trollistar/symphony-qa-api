package is.symphony.qa;

import is.symphony.qa.client.responses.BaseResponse;
import is.symphony.qa.client.responses.UsersResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetUsersTests extends BaseApiTest {

    @Test
    public void validRequest_UsersReturned() {
        BaseResponse<UsersResponse> response = client.getUsers();

        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertEquals(response.getContent().getPage(), 1);
        Assert.assertEquals(response.getContent().getPerPage(), 6);
        Assert.assertEquals(response.getContent().getTotal(), 12);
        Assert.assertEquals(response.getContent().getTotalPages(), 2);
        Assert.assertEquals(response.getContent().getAd().getCompany(), "StatusCode Weekly");
        Assert.assertEquals(response.getContent().getAd().getUrl(), "http://statuscode.org/");
        Assert.assertEquals(response.getContent().getAd().getText(), "A weekly newsletter focusing on software development, infrastructure, the server, performance, and the stack end of things.");

        Assert.assertEquals(response.getContent().getUsers().get(0).getId(), 1);
        Assert.assertEquals(response.getContent().getUsers().get(0).getEmail(), "george.bluth@reqres.in");
        Assert.assertEquals(response.getContent().getUsers().get(0).getFirstName(), "George");
        Assert.assertEquals(response.getContent().getUsers().get(0).getLastName(), "Bluth");
        Assert.assertEquals(response.getContent().getUsers().get(0).getAvatar(), "https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg");

        Assert.assertEquals(response.getContent().getUsers().get(1).getId(), 2);
        Assert.assertEquals(response.getContent().getUsers().get(1).getEmail(), "janet.weaver@reqres.in");
        Assert.assertEquals(response.getContent().getUsers().get(1).getFirstName(), "Janet");
        Assert.assertEquals(response.getContent().getUsers().get(1).getLastName(), "Weaver");
        Assert.assertEquals(response.getContent().getUsers().get(1).getAvatar(), "https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg");

        Assert.assertEquals(response.getContent().getUsers().get(2).getId(), 3);
        Assert.assertEquals(response.getContent().getUsers().get(2).getEmail(), "emma.wong@reqres.in");
        Assert.assertEquals(response.getContent().getUsers().get(2).getFirstName(), "Emma");
        Assert.assertEquals(response.getContent().getUsers().get(2).getLastName(), "Wong");
        Assert.assertEquals(response.getContent().getUsers().get(2).getAvatar(), "https://s3.amazonaws.com/uifaces/faces/twitter/olegpogodaev/128.jpg");

        Assert.assertEquals(response.getContent().getUsers().get(3).getId(), 4);
        Assert.assertEquals(response.getContent().getUsers().get(3).getEmail(), "eve.holt@reqres.in");
        Assert.assertEquals(response.getContent().getUsers().get(3).getFirstName(), "Eve");
        Assert.assertEquals(response.getContent().getUsers().get(3).getLastName(), "Holt");
        Assert.assertEquals(response.getContent().getUsers().get(3).getAvatar(), "https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg");

        Assert.assertEquals(response.getContent().getUsers().get(4).getId(), 5);
        Assert.assertEquals(response.getContent().getUsers().get(4).getEmail(), "charles.morris@reqres.in");
        Assert.assertEquals(response.getContent().getUsers().get(4).getFirstName(), "Charles");
        Assert.assertEquals(response.getContent().getUsers().get(4).getLastName(), "Morris");
        Assert.assertEquals(response.getContent().getUsers().get(4).getAvatar(), "https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg");

        Assert.assertEquals(response.getContent().getUsers().get(5).getId(), 6);
        Assert.assertEquals(response.getContent().getUsers().get(5).getEmail(), "tracey.ramos@reqres.in");
        Assert.assertEquals(response.getContent().getUsers().get(5).getFirstName(), "Tracey");
        Assert.assertEquals(response.getContent().getUsers().get(5).getLastName(), "Ramos");
        Assert.assertEquals(response.getContent().getUsers().get(5).getAvatar(), "https://s3.amazonaws.com/uifaces/faces/twitter/bigmancho/128.jpg");
    }
}
