package is.symphony.qa;

import is.symphony.qa.client.ReqresClient;
import is.symphony.qa.data.TestData;

public class BaseApiTest {
    protected static ReqresClient client = new ReqresClient();
    protected static TestData testData = new TestData();
}
