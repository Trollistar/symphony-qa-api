package is.symphony.qa;

import is.symphony.qa.client.responses.BaseResponse;
import is.symphony.qa.client.responses.DeleteUserResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteUserTests extends BaseApiTest {

    @Test
    public void validUser_204Returned() {
        BaseResponse<DeleteUserResponse> response = client.deleteUser(testData.getExistingUserId());

        // I would much prefer if Reqres returned 200 or 202 code for successful deletion,
        // presuming that the current behavior of the api is as intended.

        // However, according to https://stackoverflow.com/a/18981344, 204 status code can be
        // considered successful for DELETE requests, since there will be no data to return.

        // That is the reason why we are asserting for the 204 status code here.
        Assert.assertEquals(response.getStatusCode(), 204);
    }

    @Test
    public void invalidUser_204Returned() {
        BaseResponse<DeleteUserResponse> response = client.deleteUser(testData.getNonExistingUserId());

        // This is why.
        Assert.assertEquals(response.getStatusCode(), 204);
    }
}
