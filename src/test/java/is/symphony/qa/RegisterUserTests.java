package is.symphony.qa;

import is.symphony.qa.client.requests.RegisterUserRequest;
import is.symphony.qa.client.responses.BaseResponse;
import is.symphony.qa.client.responses.RegisterUserResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RegisterUserTests extends BaseApiTest {

    @Test
    public void validData_UserRegistered() {
        RegisterUserRequest body = new RegisterUserRequest();
        body.setEmail(testData.getExistingEmail());
        body.setPassword(testData.getValidPassword());

        BaseResponse<RegisterUserResponse> response = client.registerUser(body);

        Assert.assertEquals(response.getContent().getId(), 1);
        Assert.assertNotNull(response.getContent().getToken());
        Assert.assertEquals(response.getContent().getToken().length(), 17);
    }

    @Test
    public void missingEmailAndPassword_ErrorReturned() {
        RegisterUserRequest body = new RegisterUserRequest();

        BaseResponse<RegisterUserResponse> response = client.registerUser(body);

        Assert.assertEquals(response.getContent().getError(), "Missing email or username");
    }

    @Test
    public void missingEmail_ErrorReturned() {
        RegisterUserRequest body = new RegisterUserRequest();
        body.setPassword(testData.getValidPassword());

        BaseResponse<RegisterUserResponse> response = client.registerUser(body);

        Assert.assertEquals(response.getContent().getError(), "Missing email or username");
    }

    @Test
    public void missingPassword_ErrorReturned() {
        RegisterUserRequest body = new RegisterUserRequest();
        body.setEmail(testData.getExistingEmail());

        BaseResponse<RegisterUserResponse> response = client.registerUser(body);

        Assert.assertEquals(response.getContent().getError(), "Missing password");
    }

    @Test
    public void nonExistingUser_ErrorReturned() {
        RegisterUserRequest body = new RegisterUserRequest();
        body.setEmail(testData.getNonExistingEmail());
        body.setPassword(testData.getValidPassword());

        BaseResponse<RegisterUserResponse> response = client.registerUser(body);

        Assert.assertEquals(response.getContent().getError(), "Note: Only defined users succeed registration");
    }

}
