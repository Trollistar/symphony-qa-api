package is.symphony.qa;

import is.symphony.qa.client.responses.BaseResponse;
import is.symphony.qa.client.responses.UsersResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetUsersWithDelayTests extends BaseApiTest {
    @Test
    public void validDelay_ResponseReturnedAfterSetDelay() {
        BaseResponse<UsersResponse> response = client.getUsersWithDelay(testData.getValidDelay());

        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertEquals(response.getContent().getUsers().size(), 6);
        Assert.assertTrue(response.getResponseTime() > testData.getValidDelay() * 1000);
        Assert.assertTrue(response.getResponseTime() < (testData.getValidDelay() * 1000) + testData.getAllowedDelayDeviation());
    }

    @Test
    public void delayTooLong_ResponseInstantlyReturned() {
        BaseResponse<UsersResponse> response = client.getUsersWithDelay(testData.getInvalidDelay());

        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertEquals(response.getContent().getUsers().size(), 6);

        Assert.assertTrue(response.getResponseTime() < testData.getAllowedDelayDeviation(),
                "Request not executed instantly if the delay is over 30 seconds.");
    }
}
