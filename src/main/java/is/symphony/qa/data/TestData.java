package is.symphony.qa.data;

public class TestData {
    private String existingEmail;
    private String nonExistingEmail;
    private String validPassword;
    private int existingUserId;
    private int nonExistingUserId;
    private int validDelay;
    private int invalidDelay;
    private int allowedDelayDeviation;

    public TestData() {
        existingEmail = "george.bluth@reqres.in";
        nonExistingEmail = "tester@test.com";
        validPassword = "qwe123.";
        existingUserId = 1;
        nonExistingUserId = 2147483647;
        validDelay = 1;
        invalidDelay = 31;
        allowedDelayDeviation = 500;
    }

    public int getExistingUserId() {
        return existingUserId;
    }

    public String getNonExistingEmail() {
        return nonExistingEmail;
    }

    public int getNonExistingUserId() {
        return nonExistingUserId;
    }

    public int getValidDelay() {
        return validDelay;
    }

    public int getInvalidDelay() {
        return invalidDelay;
    }

    public int getAllowedDelayDeviation() {
        return allowedDelayDeviation;
    }

    public String getExistingEmail() {
        return existingEmail;
    }

    public String getValidPassword() {
        return validPassword;
    }
}
