package is.symphony.qa.client;

import is.symphony.qa.client.requests.RegisterUserRequest;
import is.symphony.qa.client.responses.BaseResponse;
import is.symphony.qa.client.responses.DeleteUserResponse;
import is.symphony.qa.client.responses.RegisterUserResponse;
import is.symphony.qa.client.responses.UsersResponse;

@SuppressWarnings("unchecked")
public class ReqresClient extends BaseApiClient {

    public ReqresClient() {
        super("https://reqres.in/api/");
    }

    public BaseResponse<UsersResponse> getUsers() {
        return get("users", UsersResponse.class);
    }

    public BaseResponse<UsersResponse> getUsersWithDelay(int delay) {
        return get(String.format("users?delay=%d", delay), UsersResponse.class);
    }

    public BaseResponse<RegisterUserResponse> registerUser(RegisterUserRequest body) {
        return post("register", formatPostBody(body), RegisterUserResponse.class);
    }

    public BaseResponse<DeleteUserResponse> deleteUser(int userId) {
        return delete(String.format("users/%d", userId), DeleteUserResponse.class);
    }
}
