package is.symphony.qa.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import is.symphony.qa.client.responses.BaseResponse;
import okhttp3.*;

import java.io.IOException;
import java.util.Objects;

@SuppressWarnings({"rawtypes", "unchecked"})
class BaseApiClient {
    private String baseApiUrl;

    BaseApiClient(String apiUrl) {
        baseApiUrl = apiUrl;
    }

    /**
     * Executes a GET request.
     *
     * @param endpoint Resource that will be appended to the base api url in order to perform a
     *                 request.
     * @param type     Type into which to deserialize the response if it is successful.
     * @return Completely deserialized response body as well as status code and any message
     * available.
     */
    <T> BaseResponse get(String endpoint, Class<T> type) {
        Request request = new Request.Builder()
                .url(baseApiUrl + endpoint)
                .get()
                .build();

        return performRequest(request, type);
    }

    <T> BaseResponse delete(String endpoint, Class<T> type) {
        Request request = new Request.Builder()
                .url(baseApiUrl + endpoint)
                .delete()
                .build();

        return performRequest(request, type);
    }

    /**
     * Executes a POST request.
     *
     * @param endpoint Resource that will be appended to the base api url in order to perform a
     *                 request.
     * @param body     Json that will be used as post body fo the request.
     * @param type     Type into which to deserialize the response if it is successful.
     * @return Completely deserialized response body as well as status code and any message
     * available.
     */
    <T> BaseResponse post(String endpoint, RequestBody body, Class<T> type) {
        Request request = new Request.Builder()
                .url(baseApiUrl + endpoint)
                .post(body)
                .build();

        return performRequest(request, type);
    }

    /**
     * Format the body object into valid json string.
     *
     * @param body Object that will be serialized into JSON string.
     * @return Request body formatted for okhttp3 use.
     */
    RequestBody formatPostBody(Object body) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonBody = "";
        try {
            jsonBody = mapper.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return RequestBody.create(MediaType.get("application/json; charset=utf-8"), jsonBody);
    }

    /**
     * @param request Formed request to be executed.
     * @param type    Type into which to deserialize the response if it is successful.
     * @return Completely deserialized response body as well as status code and any message
     * available.
     */
    private <T> BaseResponse performRequest(Request request, Class<T> type) {
        OkHttpClient client = new OkHttpClient();
        BaseResponse genericResponse = new BaseResponse<>();

        // Execute the previously formed request
        try (Response response = client.newCall(request).execute()) {

            /* Handle response deserialization here as well, due to the limitation of
            OkHttpClient that the body can be accessed only once*/
            genericResponse.setStatusCode(response.networkResponse() != null ? response.networkResponse().code() : 0);
            genericResponse.setResponseTime(response.receivedResponseAtMillis() - response.sentRequestAtMillis());
            // Deserialize the response
            ObjectMapper mapper = new ObjectMapper();
            genericResponse.setContent(mapper.readValue(Objects.requireNonNull(response.body()).string(), type));

            return genericResponse;

        } catch (IOException e) {
            genericResponse.setContent("Something went wrong when parsing the response.");
            return genericResponse;
        }
    }
}

