package is.symphony.qa.client.responses;

public class AdResponse {
    private String company;
    private String text;
    private String url;

    public String getCompany() {
        return company;
    }

    public String getText() {
        return text;
    }

    public String getUrl() {
        return url;
    }
}
