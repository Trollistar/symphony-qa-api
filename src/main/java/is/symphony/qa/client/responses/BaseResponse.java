package is.symphony.qa.client.responses;

public class BaseResponse<T> {
    private long responseTime;
    private int statusCode;
    private T content;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(long setResponseTime) {
        this.responseTime = setResponseTime;
    }
}

