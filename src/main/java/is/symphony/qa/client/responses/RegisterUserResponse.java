package is.symphony.qa.client.responses;

public class RegisterUserResponse {
    private int id;
    private String token;
    private String error;

    public int getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public String getError() {
        return error;
    }
}
