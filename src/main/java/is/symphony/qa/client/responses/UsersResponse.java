package is.symphony.qa.client.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class UsersResponse {
    @JsonProperty("per_page")
    private int perPage;
    private int total;
    private AdResponse ad;
    @JsonProperty("data")
    private List<UserResponse> users;
    private int page;
    @JsonProperty("total_pages")
    private int totalPages;

    public int getPerPage() {
        return perPage;
    }

    public int getTotal() {
        return total;
    }

    public AdResponse getAd() {
        return ad;
    }

    public List<UserResponse> getUsers() {
        return users;
    }

    public int getPage() {
        return page;
    }

    public int getTotalPages() {
        return totalPages;
    }
}